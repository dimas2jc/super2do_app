$(document).ready(function(){
	'use strict';
    window.onload = getData(baseUrl, "GET")

    $("#all").on("click", function(){
        getData(baseUrl, "GET")
    })

    $("#active").on("click", function(){
        getData(baseUrl, "GET", 0)
    })

    $("#completed").on("click", function(){
        getData(baseUrl, "GET", 1)
    })

    $("#toggle-all").on("click", function(){
        $.ajax({
            type: 'GET',
            url: baseUrl+"/update_semua",
            data: {
                _token: csrfToken,
            },
            success: function (results) {
                console.log(results);
                location.reload();
            }
        });
    })

    var id = null
    $(".check").on("click", function(){
        id = $(this).val()
        if(this.checked) {
            updateStatus(id, 1)
        }
        else{
            updateStatus(id, 0)
        }
    })

    function checkbox(){
        $(".check").on("click", function(){
            id = $(this).val()
            if(this.checked) {
                updateStatus(id, 1)
            }
            else{
                updateStatus(id, 0)
            }
        })
    }

    $(".clear-completed").on("click", function(){
        $.ajax({
            type: 'GET',
            url: baseUrl+"/delete_by_status",
            data: {
                _token: csrfToken,
            },
            success: function (results) {
                console.log(results);
                location.reload();
            }
        });
    })

    $(".new-todo").on('keypress',function(e) {
        if(e.which == 13) {
            let title = $(this).val()
            $.ajax({
                type: 'POST',
                url: baseUrl,
                data: {
                    _token: csrfToken,
                    title: title
                },
                success: function (results) {
                    console.log(results);
                    location.reload();
                }
            });
        }
    });

	function getData(url, method, status = null){
        $.ajax({
            type: method,
            url: url,
            data: {
                _token: csrfToken,
                status : status
            },
            success: function (results) {
                $(".data div").remove()
                $("#total").html(results.total)

                var content = ""
                var selesai = null
                var ceklist = null
                for(let i = 0; i < results.data.length; i++){
                    if(results.data[i].status == 1){
                        selesai = "completed"
                        ceklist = "checked"
                    }

                    content += `<div>
                                    <li id="`+results.data[i].id+`" class="`+selesai+`">
                                        <div class="view">
                                            <input class="toggle check" name="list_id" value="`+results.data[i].id+`" type="checkbox" `+ceklist+`>
                                            <label>`+results.data[i].title+`</label>
                                        </div>
                                    </li>
                                </div>`

                    selesai = null
                    ceklist = null
                }
                $('.data').append(content)
                checkbox()

                var completed = false
                for(let j = 0; j < results.data.length; j++){
                    if(results.data[j].status == 1){
                        completed = true
                        break
                    }
                }

                if((status == null || status == 1) && completed == true){
                    $(".clear-completed").css("display", "block")
                }
                else{
                    $(".clear-completed").css("display", "none")
                }
            }
        });
    }

    function updateStatus(id, status){
        $.ajax({
            type: 'PUT',
            url: baseUrl+"/"+id,
            data: {
                _token: csrfToken,
                status: status
            },
            success: function (results) {
                console.log(results);
                if(status === 1){
                    $("#"+id).addClass("completed")
                    $(".clear-completed").css("display", "block")
                }
                else{
                    $("#"+id).removeClass("completed")
                    $(".clear-completed").css("display", "none")
                }
            }
        });
    }

});
