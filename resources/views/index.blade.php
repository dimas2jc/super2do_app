<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<title>Template • Todo</title>
		<link rel="stylesheet" href="{{asset('css/base.css')}}">
		<link rel="stylesheet" href="{{asset('css/index.css')}}">
		<!-- CSS overrides - remove if you don't need it -->
		<link rel="stylesheet" href="{{asset('css/app.css')}}">
	</head>
	<body>
		<section class="todoapp">
			<header class="header">
				<h1>Super2Do</h1>
				<input class="new-todo" placeholder="What needs to be done?" autofocus>
			</header>
			<!-- This section should be hidden by default and shown when there are todos -->
			<section class="main">
				<input id="toggle-all" class="toggle-all" type="checkbox">
				<label for="toggle-all">Mark all as complete</label>

				<ul class="todo-list">
					<!-- These are here just to show the structure of the list items -->
					<!-- List items should get the class `editing` when editing and `completed` when marked as completed -->
                    <div class="data">
					{{-- <li class="completed">
						<div class="view">
							<input class="toggle" type="checkbox">
							<label>Taste JavaScript</label>
							<button class="destroy"></button>
						</div>
					</li> --}}
                    </div>
				</ul>
			</section>
			<!-- This footer should be hidden by default and shown when there are todos -->
			<footer class="footer">
				<!-- This should be `0 items left` by default -->
				<span class="todo-count"><strong id="total">0</strong> item left</span>
				<!-- Remove this if you don't implement routing -->
				<ul class="filters">
					<li>
						<span style="cursor: pointer" id="all">All</span>
					</li>
					<li style="margin-left: 10px; margin-right: 10px">
						<span style="cursor: pointer" id="active">Active</span>
					</li>
					<li>
						<span style="cursor: pointer" id="completed">Completed</span>
					</li>
				</ul>
				<!-- Hidden if no completed items are left ↓ -->
				<button class="clear-completed" style="display: none">Clear completed</button>
			</footer>
		</section>
        <!-- Scripts here. Don't remove ↓ -->
        <script src="{{asset('js/jquery.min.js')}}"></script>
		<script src="{{asset('js/app.js')}}"></script>
        <script>
            const csrfToken = $('meta[name="csrf-token"]').attr('content')
            const baseUrl = "api/list"
        </script>
	</body>
</html>
