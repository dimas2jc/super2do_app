<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ListController extends Controller
{
    /**
     * Date : 07-02-2022
     * Description : get lists data
     * Developer : Dimas Ihsan
     * Status : Create
     */
    public function index(Request $request)
    {
        $data = DB::table('list');
        if($request->filled('status')){
            if($request->status == 0){ // Active
                $data = $data->where('status', 0);
            }
            else if($request->status == 1){ // Completed
                $data = $data->where('status', 1);
            }
        }
        $dt['data'] = $data->get();
        $dt['total'] = count($dt['data']);

        return response()->json($dt, 200);
    }

    /**
     * Date : 07-02-2022
     * Description : Menyimpan data list baru
     * Developer : Dimas Ihsan
     * Status : Create
     */
    public function store(Request $request)
    {
        DB::table('list')->insert([
            'title' => $request->title,
            'status' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        return response()->json(["message" => "success"], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Date : 07-02-2022
     * Description : Menyimpan perubahan list
     * Developer : Dimas Ihsan
     * Status : Create
     */
    public function update(Request $request, $id)
    {
        DB::table('list')->where('id', $id)->update([
            // 'title' => $request->title,
            'status' => $request->status,
            'updated_at' => Carbon::now()
        ]);

        return response()->json(["message" => "success"], 201);
    }

    /**
     * Date : 07-02-2022
     * Description : Menghapus list sesuai id
     * Developer : Dimas Ihsan
     * Status : Create
     */
    public function destroy($id)
    {
        DB::table('list')->where('id', $id)->delete();

        return response()->json(["message" => "success"], 200);
    }

    /**
     * Date : 07-02-2022
     * Description : Mengganti status list
     * Developer : Dimas Ihsan
     * Status : Create
     */
    public function updateStatus(Request $request)
    {
        $new_status = null;
        if($request->status == 0){
            $new_status = 1;
        }
        else if($request->status == 1){
            $new_status = 0;
        }

        DB::beginTransaction();
        DB::table('list')->where('status', $request->status)->update([
            'status' => $new_status,
            'updated_at' => Carbon::now()
        ]);
        DB::commit();

        return response()->json(["message" => "success"], 201);
    }

    /**
     * Date : 07-02-2022
     * Description : Mengganti status list
     * Developer : Dimas Ihsan
     * Status : Create
     */
    public function updateSemuaStatus()
    {
        DB::table('list')->where('status', 0)->update([
            'status' => 1,
            'updated_at' => Carbon::now()
        ]);

        return response()->json(["message" => "success"], 200);
    }

    /**
     * Date : 07-02-2022
     * Description : Menghapus list dengan status completed
     * Developer : Dimas Ihsan
     * Status : Create
     */
    public function deleteByStatus()
    {
        DB::table('list')->where('status', 1)->delete();

        return response()->json(["message" => "success"], 200);
    }
}
