## __Instalasi__

<h4>Copy file .env.example dan beri nama .env , sesuaikan konfigurasi database</h4>

```sh
cp .env.example .env
```
<h4>Install composer dependencies (vendor)</h4>

```sh
composer install
```
<h4>Generate application key</h4>

```sh
php artisan key:generate
```
<h4>Jalankan migration</h4>

```sh
php artisan migrate
```
<h5>Untuk Confirm Add List Tekan Enter</h5>
